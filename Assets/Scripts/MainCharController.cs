﻿using UnityEngine;
using System.Collections;

public class MainCharController : MonoBehaviour
{
    public float horizontalSpeed = 5f;
    public float jumpSpeed = 2f;
    public float jumpDuration = 1f;
    public int maxMidAirJumpCount = 1;
    public float maxFallSpeed = 5f;

    public Transform groundCheckFront;
    public Transform groundCheckMid;
    public Transform groundCheckBack;
    public Transform frontCheck1;
    public Transform frontCheck2;
    public Transform frontCheck3;
    public Transform frontCheck4;
    public Transform frontCheckVertical;
    public Transform topCheckFront;
    public Transform topCheckMid;
    public Transform topCheckBack;
    public LayerMask whatIsGround;

    private bool facingRight = false;
    private float h = 0f;
    public bool jumping = false;
    public float jumpTimeLeft = 0f;
    public int jumpCountLeft = 0;

    public bool isGrounded = true;
    public bool canMoveUp = true;
    public bool canMoveForward = true;

    public Vector2 velocity;

    private Rigidbody2D rb2d;
    private Animator anim;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
	
    void Update()
    {
        if (Time.timeScale == 0) return;

        h = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Fire1"))
            anim.SetTrigger("attack");

        if (!jumping)
        {
            jumping = Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") > 0f;
            if (jumping)
            {
                if (jumpCountLeft > 0 && canMoveUp)
                {
                    jumpTimeLeft = jumpDuration;
                    if (!isGrounded)
                        jumpCountLeft--;
                    if (!anim.GetCurrentAnimatorStateInfo(0).IsName("MC_Attack"))
                        anim.Play("MC_Jump", -1, 0);
                }
            }
        }
        else
        {
            jumping = !Input.GetButtonUp("Vertical");
            if (!jumping)
            {
                jumpTimeLeft = 0f;
            }
        }
    }

    void FixedUpdate()
    {
        if (h < 0 && facingRight)
            Flip();
        if (h > 0 && !facingRight)
            Flip();
        CheckForward();
        if (Mathf.Abs(h) > Mathf.Epsilon && canMoveForward)
        {
            rb2d.velocity = new Vector2(facingRight ? horizontalSpeed : -horizontalSpeed, rb2d.velocity.y);
            anim.SetBool("running", true);
        }
        else
        {
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            anim.SetBool("running", false);
        }


        bool preGrounded = isGrounded;
        CheckGrounded();
        CheckTop();
        if (isGrounded)
        {
            jumpCountLeft = maxMidAirJumpCount;
            if (!preGrounded)
                jumping = false;
        }
        if (!canMoveUp)
            jumpTimeLeft = 0f;
        if (jumping && canMoveUp)
        {
            if (jumpTimeLeft > 0f)
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, jumpSpeed);
                jumpTimeLeft -= Time.fixedDeltaTime;
            }
        }

        if (rb2d.velocity.y < -maxFallSpeed)
            rb2d.velocity = new Vector2(rb2d.velocity.x, -maxFallSpeed);

        anim.SetFloat("vertical", rb2d.velocity.y);
        velocity = rb2d.velocity;
    }

    void Flip()
    {
        Vector3 temp = transform.localScale;
        temp.x *= -1f;
        transform.localScale = temp;
        facingRight = !facingRight;
    }

    void CheckGrounded()
    {
        isGrounded = Physics2D.Raycast(groundCheckFront.position, Vector2.down, 0.05f, whatIsGround)
            || Physics2D.Raycast(groundCheckMid.position, Vector2.down, 0.05f, whatIsGround)
            || Physics2D.Raycast(groundCheckBack.position, Vector2.down, 0.05f, whatIsGround);
    }

    void CheckTop()
    {
        canMoveUp = !(Physics2D.Raycast(topCheckFront.position, Vector2.up, 0.05f, whatIsGround)
            || Physics2D.Raycast(topCheckMid.position, Vector2.up, 0.05f, whatIsGround)
            || Physics2D.Raycast(topCheckBack.position, Vector2.up, 0.05f, whatIsGround));
    }

    void CheckForward()
    {
        Vector2 forward = facingRight ? Vector2.right : Vector2.left;
        canMoveForward = !(Physics2D.Raycast(frontCheck1.position, forward, 0.05f, whatIsGround)
            || Physics2D.Raycast(frontCheck2.position, forward, 0.05f, whatIsGround)
            || Physics2D.Raycast(frontCheck3.position, forward, 0.05f, whatIsGround)
            || Physics2D.Raycast(frontCheck4.position, forward, 0.05f, whatIsGround)
            || Physics2D.Raycast(frontCheckVertical.position, Vector2.down, .8f, whatIsGround));
    }
}
