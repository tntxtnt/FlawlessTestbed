﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InGameMenu : MonoBehaviour
{
    public GameObject menuPanel;
    public GameObject optionsPanel;
 
    private Canvas ui;

    void Awake()
    {
        ui = GetComponent<Canvas>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 0)
                Unpause();
            else if (Time.timeScale == 1)
                Pause();
        }
    }

    void OnEnable()
    {
        ToPauseMenu();
    }

    void Pause()
    {
        Time.timeScale = 0;
        ui.enabled = true;
    }

    void Unpause()
    {
        ToPauseMenu();
        Time.timeScale = 1;
        ui.enabled = false;
    }

    void ToMainMenu()
    {
        Debug.Log("Application.Quit()");
    }

    void ToOptionsMenu()
    {
        menuPanel.SetActive(false);
        optionsPanel.SetActive(true);
    }

    void ToPauseMenu()
    {
        optionsPanel.SetActive(false);
        menuPanel.SetActive(true);
    }
}
