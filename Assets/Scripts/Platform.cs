﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour
{
    public EdgeCollider2D real;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Platform"))
            real.enabled = true;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Platform"))
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
                real.enabled = false;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Platform"))
            real.enabled = false;
    }
}
